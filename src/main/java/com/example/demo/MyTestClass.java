package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyTestClass {

	@Value("${spring.demo.username}")
	private String userName;

	@Value("${spring.demo.email}")
	private String email;

	@Value("${spring.demo.password}")
	private String password;

	@RequestMapping("/getValues")
	public String printValues() {
		System.out.println("User Name is : " + userName + "Email Id is : " + email);
		return userName + " " + password;

	}

}
