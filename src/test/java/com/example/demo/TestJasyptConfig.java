package com.example.demo;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;

@SpringBootTest
public class TestJasyptConfig {

	@Test
	public void testEncryptString()
	{
		PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
		SimpleStringPBEConfig config = new SimpleStringPBEConfig();

		config.setPassword("password"); // encryptor's private key

		//config.setAlgorithm("PBEWithMD5AndDES");
		config.setKeyObtentionIterations("1000");
		config.setPoolSize("1");
		config.setProviderName("SunJCE");
		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
		config.setStringOutputType("base64");

		encryptor.setConfig(config);
		
		String text ="Newton123";
		String encryptedText = encryptor.encrypt(text);
		System.out.println(encryptedText);
		
		String decryptedText = encryptor.decrypt(encryptedText);
		System.out.println(decryptedText); 
	}
	
	@Test
	public void test2()
	{
		PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
		SimpleStringPBEConfig config = new SimpleStringPBEConfig();

		config.setPassword("password"); // encryptor's private key

		//config.setAlgorithm("PBEWithMD5AndDES");
		config.setKeyObtentionIterations("1000");
		config.setPoolSize("1");
		config.setProviderName("SunJCE");
		config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
		config.setStringOutputType("base64");

		encryptor.setConfig(config);
		
		String text ="Password123";
		String encryptedText = encryptor.encrypt(text);
		System.out.println(encryptedText);
		
		String decryptedText = encryptor.decrypt(encryptedText);
		System.out.println(decryptedText); 
	}
}
