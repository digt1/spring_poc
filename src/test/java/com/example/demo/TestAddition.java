package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestAddition {

	private Addition addition;

	@Test
	public void testAddition() {
		addition = new Addition();
		int result = addition.addition(10, 20);

		Assertions.assertEquals(30, result);
	}
}
